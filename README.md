# Vcfporter

**PLEASE READ - FirefoxOS comes with contact import functionality, but it doesn't allow you to choose which contacts you'd like to import, as of 11/2014**
**If you only want to import all contacts, please use the built-in FirefoxOS contact importer, as it is most likely more robust**

A simple FirefoxOS Open Web app for importing V-Card Format (VCF) contact data.

VCF Importer supports VCF format 2.1, 3.0, and 4.0.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM) and [Bower](http://bower.io/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at http://localhost:4200.

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

* Update manifest version number
* `ember build --environment production`
* Ensure needed manifest is present
* `cd dist && zip -r vcfporter.zip *`
* Upload to appropriate marketplace

## Issues with VCFPorter?

Please file issues @ https://bitbucket.org/t3hmrman/vcfporter/issues

## Further Reading / Useful Links

* ember: http://emberjs.com/
* ember-cli: http://www.ember-cli.com/
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

