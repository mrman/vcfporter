import Ember from "ember";
import DS from "ember-data";

var id= 0;

var Contact = DS.Model.extend({
  name: DS.attr(),
  phoneNumbers: DS.attr(),
  addresses: DS.attr(),
  photos: DS.attr(),
  emails: DS.attr(),
  organizations: DS.attr(),
  categories: DS.attr(),
  urls: DS.attr(),
  birthday: DS.attr("date"),
  anniversary: DS.attr("date"),
  gender: DS.attr(),
  key: DS.attr(),
  jobTitle: DS.attr(),
  selected: DS.attr('boolean'),

  toggleSelected: function() {
    this.set('selected', !this.get('selected'));
    return this.get('selected');
  },

  // Get the first photo if there is one, null otherwise
  firstPhoto: Ember.computed('photos', function() {
    return _.first(this.get('photos')) || null;
  }),

  // Get the first photo if there is one, null otherwise
  firstPhoneNumber: Ember.computed('phoneNumbers', function() {
    return _.first(this.get('phoneNumbers')) || null;
  }),

  // Get the first photo if there is one, null otherwise
  firstEmail: Ember.computed('emails', function() {
    return _.first(this.get('emails')) || null;
  }),

  prettyPrintedPhoneNumber: Ember.computed('phoneNumbers', function() {
    return this.get('firstPhoneNumber') || "No Phone Number";
  }),

  // Get displayable name
  displayableName: Ember.computed('name', 'something', function() {
    return this.get('name').displayName || this.get('firstEmail').value;
  }),

  // Generate
  generateMozContact: function() {
    return {
      givenName: this.get('name').givenNames || [],
      honorificPrefix: this.get('name').honorificPrefixes || [],
      honorificSuffix: this.get('name').honorificSuffixes || [],
      nickname: this.get('name').nicknames || [],
      name: [this.get('displayableName')],
      email: this.get('emails') || [],
      url: this.get('urls') || [],
      category: this.get('categories') || [],
      adr: this.get('addresses') || [],
      tel: this.get('phoneNumbers') || [],
      org: this.get('organizations') || [],
      bday: this.get('birthday') || null,
      note: this.get('notes') || [],
      impp: this.get('impp') || [],
      anniversary: this.get('anniversary') || null,
      genderIdentity: this.get('gender'),
      jobTitle: this.get('jobTitle') || [],
      key: this.get('publicEncKey') || null
    };
  }

});

Contact.prototype.generateSerialID = function() {
  return id++;
};

export default Contact;
