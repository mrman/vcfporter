import Ember from "ember";
import DS from "ember-data";

var VCFFile = DS.Model.extend({
  fileObj: DS.attr(),
  name: DS.attr("string"),
  size: DS.attr("number"),
  type: DS.attr("string"),
  parsed: DS.attr("boolean"),
  lastModifiedDate: DS.attr("date"),
  mozFullPath: DS.attr("string"),
  selected: DS.attr('boolean'),

  toggleSelected: function() {
    this.set('selected', !this.get('selected'));
    return this.get('selected');
  },

  // Proxy calls to underlaying file object
  getAsBinary: Ember.computed("file", function() {
    return this.get("file").getAsBinary();
  }),

  baseName: Ember.computed("name", function() {
    return _.last(this.get("name").split("/"));
  }),

  getAsDataURL: Ember.computed("file", function() {
    return this.get("file").getAsDataURL();
  }),

  getAsText: function(encoding) {
    var file = this.get("file");
    return file.getAsText(file,encoding);
  }
});

export default VCFFile;
