import Ember from 'ember';

/* global VcfporterENV */

var Router = Ember.Router.extend({
  location: VcfporterENV.locationType
});

Router.map(function() {
  this.route("load-vcfs", {path: "/"});
  this.route("select-contacts");
  this.route("save-contacts");
});

export default Router;
