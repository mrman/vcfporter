/**
 * VCF Parser
 *
 * @exports VCFParser
 * @param {object} options - Options for the parser
 * @param {boolean} [options.allAtOnce] - Only send out contacts when they are all parsed
 * @param {boolean} [options.requiredFields] - RequireOptions for the parser
 */


function VCFParser(options) {
  var self = this;
  self.options = options || {};
  self.state = {
    vcfVersions: [],
    currentContact: {}
  };
}

VCFParser.prototype._NAME_SEGMENTS = ["familyNames","givenNames","additionalNames","honorificPrefixes","honorificSuffixes"];
VCFParser.prototype._VALID_TYPES = ["BEGIN", "N","FN","EMAIL","TEL", "ADR", "PHOTO", "END"];
VCFParser.prototype._VALID_IMG_EXTENSIONS = ['PNG', 'JPEG', 'GIF'];
VCFParser.prototype._VALID_TEL_TYPES = ['WORK', 'FAX', 'HOME', 'CELL', 'PREF', 'VOICE'];
VCFParser.prototype._VALID_ADR_TYPES = ['WORK', 'HOME', 'PREF'];
VCFParser.prototype._VALID_GENDERS = ['male', 'female', 'other', 'none', 'unknown'];

/**
 * Check if a line (in a VCF file) has a valid
 *
 * @param {string} line - The line to be checked
 */
VCFParser.prototype.getType = function(line) {
  var possibleType = line
        .split(/:/)
        .shift()
        .split(/;/)
        .shift();

  if (this._VALID_TYPES.indexOf(possibleType) >= 0) {
    return possibleType;
  }

  return null;
};

/**
 * External-facting function for parsing a vCard file
 *
 * @param {object} f - File Object to be read and parsed
 * @param {function} cb - Callback to be called after parsing has completed
 * @param {function} endCb - Callback to call after all contacts have been loaded
 */
VCFParser.prototype.parse = function(f, cb, endCb) {
  // Create a file reader to parse the VCF File
  var self = this;
  var reader = new FileReader();

  // Call the given call back after loading has finished
  reader.onloadend = function(e) {
    // Marshal vCard raw text into lines
    var rawText = e.target.result;
    var lines = rawText.split(/\n/);

    // Parse lines
    self._parse(lines, cb, endCb);
  };
  reader.readAsText(f);
};

/**
 * Internal main vCard parser function
 *
 * @param {list} lines - Lines of the vCard file
 * @param {function} cb - Callback to call after contact(s) (depending on options)  have been loaded
 * @param {function} endCb - Callback to call after all contacts have been loaded
 * @returns A list of javascript objects that represent contacts
 */
VCFParser.prototype._parse = function(lines, cb, endCb) {
  var parser = this;
  var contacts = [];
  var lineTokens = [];

  var rawData = "";

  for (var lineNum = 0; lineNum < lines.length; lineNum++) {

    // Skip empty lines
    if (lines[lineNum].trim().length === 0) { continue; }

    // Get tokens on line (if not in the middle of some multi-line value, expect 2 tokens)
    lineTokens = lines[lineNum].split(/:/);
    if (lineTokens.length < 2) {
      throw new Error("Unexpected # of tokens on l" + lineNum + ":[" + lines[lineNum] + "]");
    }

    // Get left and right hand side of ":"
    var lhs = lineTokens.shift();
    var rhs = lineTokens;
    var rhsFirst = rhs[0].trim();

    // lhs may contain some qualifiers, along with the expected information type
    var lhsQualifiers = lhs.split(/;/);
    var type = lhsQualifiers.shift();

    // Parse LHS qualifiers if present
    var qualifiers = {};
    if (lhsQualifiers.length > 0) {
      qualifiers = parser.state.currentInfoQualifiers = parser._parseQualifiers(type, lhsQualifiers);
    }

    // Variable will be used by various cases
    rawData = "";

    // Switch on LHS type
    switch (type) {

    case "BEGIN": // Start of a vCard
      if (typeof parser.state.parsingContact !== "undefined" && parser.state.parsingContact) {
        throw new Error("Parse error: Expected a vCard to have ended before another began.");
      }

      parser.state.parsingContact = true;
      parser.state.currentContact = {
        name: {
          displayName: null,
          familyNames: [],
          givenNames: [],
          additionalNames: [],
          honorificPrefixes: [],
          honorificSuffixes: []
        },
        phoneNumbers: [],
        addresses: [],
        categories: [],
        photos: [],
        emails: [],
        urls: [],
        uids: [],
        impp: [],
        bday: null,
        rev: null,
        note: null,
        prodid: null,
        class: null,
        labels: [],
        organizations: []
      };

      break;

    case "VERSION": // Version of vCard for the contact
      parser.state.currentContact.vcfVersion = rhsFirst;

      // Add the version to witnessed versions
      if (parser.state.vcfVersions.indexOf(rhsFirst) < 0) {
        parser.state.vcfVersions.push(rhsFirst);
      }
      break;

    case "N": // Name
      rawData = rhsFirst;

      // If charset/encoding is set, line may be broken up, otherwise normal parsing
      if ('charset' in qualifiers &&
          qualifiers.charset === "UTF-8" &&
          'encoding' in qualifiers &&
          qualifiers.encoding === "QUOTED-PRINTABLE") {

        // Read and concatenate lines while we don't see lines with a valid type
        lineNum++;
        while (lineNum < lines.length && parser.getType(lines[lineNum]) == null) {
          rawData += lines[lineNum].trim();
          lineNum++;
        }
        lineNum--; // Move back one line so for-loop picks up the right line

        // Decode the UT8 QUOTED-PRINTABLE code into a UTF8 contact name
        rawData = utf8QuotedPrintableDecode(rawData);

      }

      // Parse name segments (expecting "<familyName>;<givenName>;<additionalName>;<HonorificPrefix>;<HonorificSuffix>;")
      var nameSegments = rawData.split(/;/);
      for (var i = 0; i < nameSegments.length; i++) {
        if (nameSegments[i].trim() !== "") {
          parser.state.currentContact.name[parser._NAME_SEGMENTS[i]].push(nameSegments[i]);
        }
      }

      break;

    case "FN": // Full name
      rawData = rhsFirst;
      parser.state.currentContact.name.displayName = rawData;

      // Handle quoted printable UTF8
      if ('charset' in qualifiers &&
          qualifiers.charset === "UTF-8" &&
          'encoding' in qualifiers &&
          qualifiers.encoding === "QUOTED-PRINTABLE") {

        // Read and concatenate lines while we don't see lines with a valid type
        lineNum++;
        while (lineNum < lines.length && parser.getType(lines[lineNum]) == null) {
          rawData += lines[lineNum].trim();
          lineNum++;
        }

        // Decode the UT8 QUOTED-PRINTABLE code into a UTF8 contact name
        parser.state.currentContact.name.displayName = utf8QuotedPrintableDecode(rawData);
      }

      break;

    case "NICKNAME": // Nickname
      rawData = rhsFirst;

      // Handle quoted printable UTF8
      if ('charset' in qualifiers &&
          qualifiers.charset === "UTF-8" &&
          'encoding' in qualifiers &&
          qualifiers.encoding === "QUOTED-PRINTABLE") {

        // Read and concatenate lines while we don't see lines with a valid type
        lineNum++;
        while (lineNum < lines.length && parser.getType(lines[lineNum]) == null) {
          rawData += lines[lineNum].trim();
          lineNum++;
        }

        // Decode the UT8 QUOTED-PRINTABLE code into a UTF8 contact name
        rawData = utf8QuotedPrintableDecode(rawData);
      }

      parser.state.currentContact.name.nicknames.push(rawData);
      break;

    case "TEL": // Home/Work/Cell/Voice telephone number
      // Create a list of the types the number was qualified with
      var types = [];
      for (var t = 0; t < parser._VALID_TEL_TYPES.length; t++) {
        if (parser._VALID_TEL_TYPES in qualifiers) {
          types.push(parser._VALID_TEL_TYPES[t]);
        }
      }

      // Add the phone number to the list of phone numbers for this contact
      parser.state.currentContact.phoneNumbers.push({
        carrier: "",
        types: types,
        preferred: 'pref' in qualifiers || false,
        value: rhsFirst
      });
      break;

    case "NOTE": // Nickname
      rawData = rhsFirst;

      // Handle quoted printable UTF8
      if ('encoding' in qualifiers &&
          qualifiers.encoding === "QUOTED-PRINTABLE") {

        // Read and concatenate lines while we don't see lines with a valid type
        lineNum++;
        while (lineNum < lines.length && parser.getType(lines[lineNum]) == null) {
          rawData += lines[lineNum].trim();
          lineNum++;
        }
      }

      parser.state.currentContact.note = rawData;
      break;

    case "UID":
      parser.state.currentContact.uids.push(rhsFirst);
      break;

    case "BDAY":
      // Expecting BDAY to be formatted like: <year><month><day> (EX: 19701030)
      parser.state.currentContact.bday = new Date(parseInt(rhsFirst.slice(0,4)),
                                                  parseInt(rhsFirst.slice(4,6)),
                                                  parseInt(rhsFirst.slice(6)));
      break;

    case "LABEL":
      parser.state.currentContact.labels.push(rhsFirst);
      break;

    case "PRODID":
      parser.state.currentContact.prodid = rhsFirst;
      break;

    case "CLASS":
      parser.state.currentContact.class = rhsFirst;
      break;

    case "REV":
      // Expecting REV to be formatted like: 20121201T134211Z
      parser.state.currentContact.rev = new Date(rhsFirst);
      break;

    case "ADR":
      parser.handleADR(parser.state.currentContact, qualifiers, rhsFirst);
      break;

    case "EMAIL": // Email address
      parser.handleEMAIL(parser.state.currentContact, qualifiers, rhsFirst);
      break;

    case "URL": // Website/URL
      parser.handleURL(parser.state.currentContact, qualifiers, rhsFirst);
      break;

    case "ORG": // Organization
      parser.state.currentContact.organizations.push(rhsFirst);
      break;

    case "IMPP": // Instant messaging addresses
      parser.handleIMPP(parser.state.currentContact, qualifiers, rhsFirst);
      break;

    case "GENDER":
      rawData = rhsFirst.toLowerCase();
      if (parser._VALID_GENDERS.indexOf(rawData) >= 0) {
        parser.state.currentContact.gender = rawData;
      }
      break;

    case "CATEGORIES": // Categories
      // Add all the non-empty categories
      rawData = rhsFirst.split(",");
      for (var c = 0; c < rawData.length; c++) {
        if (rawData[c].trim() !== "") { parser.state.currentContact.categories.push(rawData[c].trim()); }
      }
      break;

    case "END": // End of a vCard
      parser.state.parsingContact = false;

      // If requiredFields have been specified, ensure they have been provided
      // if any required fields are missing, skip adding this contact
      var skip = false;
      if (typeof parser.options.requiredFields !== "undefined" &&
          parser.options.requiredFields.length > 0) {
        for (var j = 0; !skip && j < parser.options.requiredFields.length; j++) {
          var requiredField = parser.options.requiredFields[j];
          // If required field is missing or null (some are initialzed to null) then skip
          if (!(requiredField in parser.state.currentContact) ||
              parser.state.currentContact[requiredField] == null) {
            skip = true;
          }
        }
      }

      // Push the contact that was just created if allAtOnce option is specified,
      // otherwise call the callback for an individual parsed contact
      if (!skip) {
        if (parser.options.allAtOnce) {
          contacts.push(parser.state.currentContact);
        } else {
          cb(parser.state.currentContact);
        }
      }

      break;

    case "PHOTO":
      rawData = "";

      // Ensure we receive a valid ext
      if (parser._VALID_IMG_EXTENSIONS.indexOf(qualifiers.ext) < 0) {
        throw new Error("Invalid/missing photo extension [" + qualifiers.ext + "] while parsing contact.");
      }

      // Add expected/relevant qualifiers (might be dangerous to default to PNG...)
      var encoding = qualifiers.encoding || "BASE-64";
      var ext = qualifiers.ext || "PNG";

      // Add current trimmed rhs & Read to empty line to get all the data for the photo
      rawData = rhsFirst.replace(/\n$/,"");
      lineNum++;
      while (lines[lineNum].trim().length > 0 && lineNum < lines.length) {
        rawData += lines[lineNum].trim().replace(/\n$/,"");
        lineNum++;
      }

      var dataURI = "data:image/" + ext.toLowerCase() + ";" + encoding.toLowerCase() + "," + rawData;
      parser.state.currentContact.photos.push(dataURI);
      break;


    default:
      throw new Error("Parse error: Unexpected LHS [" + lhs + "]");

    }
  }

  // Call the callback with the contacts array that was parsed
  if (typeof parser.options.allAtOnce !== "undefined" &&
      parser.options.allAtOnce &&
      cb) {
    cb(contacts);
  }

  // Call end callback if allAtOnce option is undefined or false
  if (typeof parser.options.allAtOnce === "undefined" || !parser.options.allAtOnce) {
    if (endCb) { endCb(); }
  }
};


/**
 * Parse qualifiers on information lines of vCard contacts
 *
 * @param {string} type - Type of the information line (ex. N, FN, PHOTO)
 * @param {list} qualifiers - List of value or key/value pair strings
 * @returns {object} Parsed qualifiers, with type aware keys
 */
VCFParser.prototype._parseQualifiers = function(type, qualifiers) {
  var self = this;
  var qualifierObject = {};

  for (var i = 0; i < qualifiers.length; i++) {
    var qualifierTokens = qualifiers[i].split(/=/);

    // If qualifier is a key value pair, value is the second element, identifier is first
    // If qualifier is just a value, identifier is the element, value is the element
    var qualifierType = (qualifierTokens.length > 1) ? "kv" : "v";
    var qualifierValue = (qualifierType === "kv") ? qualifierTokens[1] : qualifierTokens[0];
    var qualifierIdentifier = (qualifierType === "kv") ? qualifierTokens[0] : qualifierValue;

    // Perform transform of qualifierObject
    try {
      self._QUALIFIERS_TREE[type][qualifierType][qualifierIdentifier](qualifierObject, qualifierValue);
    } catch (e) {
      // Swallow error
    }

  } // /for

  return qualifierObject;
};

// Simple utility function for generating simple qualifier functions
VCFParser.prototype._simpleQualifierFnGenerator = function(objectKey) {
  return function(q,v) {
    q[objectKey] = v;
  };
};

// Tree that represents and contains functions for all known parse qualifiers,
// access structure: type -> (kv | v) -> qualifierIdentifier -> function(qualifierObject, [value])
VCFParser.prototype._QUALIFIERS_TREE = {
  N: {
    kv: {
      CHARSET: VCFParser.prototype._simpleQualifierFnGenerator("charset"),
      ENCODING: VCFParser.prototype._simpleQualifierFnGenerator("encoding")
    }
  },

  FN: {
    kv: {
      CHARSET: VCFParser.prototype._simpleQualifierFnGenerator("charset"),
      ENCODING: VCFParser.prototype._simpleQualifierFnGenerator("encoding")
    }
  },

  NICKNAME: {
    kv: {
      CHARSET: VCFParser.prototype._simpleQualifierFnGenerator("charset"),
      ENCODING: VCFParser.prototype._simpleQualifierFnGenerator("encoding")
    }
  },

  TEL: {
    v: {
      WORK: VCFParser.prototype._simpleQualifierFnGenerator("work"),
      FAX: VCFParser.prototype._simpleQualifierFnGenerator("fax"),
      HOME: VCFParser.prototype._simpleQualifierFnGenerator("home"),
      CELL: VCFParser.prototype._simpleQualifierFnGenerator("cell"),
      PREF: VCFParser.prototype._simpleQualifierFnGenerator("pref"),
      VOICE: VCFParser.prototype._simpleQualifierFnGenerator("voice")
    }
  },

  NOTE: {
    kv: {
      ENCODING: VCFParser.prototype._simpleQualifierFnGenerator("encoding")
    }
  },

  EMAIL: {
    v: {
      PREF: VCFParser.prototype._simpleQualifierFnGenerator("pref"),
      HOME: VCFParser.prototype._simpleQualifierFnGenerator("home"),
      "X-INTERNET": VCFParser.prototype._simpleQualifierFnGenerator("x-internet")
    }
  },

  ADR: {
    v: {
      HOME: VCFParser.prototype._simpleQualifierFnGenerator("home"),
      PREF: VCFParser.prototype._simpleQualifierFnGenerator("pref"),
      "X-OTHER": VCFParser.prototype._simpleQualifierFnGenerator("x-other")
    }
  },

  PHOTO: {
    kv: {
      ENCODING: VCFParser.prototype._simpleQualifierFnGenerator("encoding")
    },
    v: {
      JPEG: function(q,v) { q.ext = v; }
    }
  }

};

/**
 * Handle reception of ADR (addresses)
 *
 * @param {object} contact - The contact to update with new information
 * @param {object} qualifiers - Parsed kv/v qualifiers (ex. HOME, WORK)
 * @param {string} rawData - Data on the line
 */
VCFParser.prototype.handleADR = function(contact, qualifiers, rawData) {
  var parser = this;
  // Create a list of the types the number was qualified with
  var types = [];
  for (var t = 0; t < parser._VALID_TEL_TYPES.length; t++) {
    if (parser._VALID_TEL_TYPES in qualifiers) {
      types.push(parser._VALID_TEL_TYPES[t]);
    }
  }

  // Parse name segments
  var nameSegments = rawData.split(/;/);
  parser.state.currentContact.addresses.push({
    countryName: nameSegments[6],
    locality: nameSegments[3],
    postalCode: nameSegments[5],
    preferred: 'pref' in qualifiers,
    region: nameSegments[4],
    streetAddress: nameSegments[2],
    types: types
  });
};

/**
 * Handle reception of EMAIL (email addresses)
 * 
 * @param {object} contact - The contact to update with new information
 * @param {object} qualifiers - Parsed kv/v qualifiers (ex. HOME, WORK)
 * @param {string} rawData - Data on the line
*/
VCFParser.prototype.handleEMAIL = function(contact, qualifiers, rawData) {
  var parser = this;

  // Create a list of the types the email was qualified with
  var types = [];
  for (var t = 0; t < parser._VALID_TEL_TYPES.length; t++) {
    if (parser._VALID_TEL_TYPES in qualifiers) {
      types.push(parser._VALID_TEL_TYPES[t]);
    }
  }

  // Add the email address to the list of email addresses for this contact
  parser.state.currentContact.emails.push({
    types: types,
    preferred: qualifiers.pref,
    value: rawData
  });
};

/**
 * Handle reception of URL (website)
 *
 * @param {object} contact - The contact to update with new information
 * @param {object} qualifiers - Parsed kv/v qualifiers (ex. HOME, WORK)
 * @param {string} rawData - Data on the line
 */
VCFParser.prototype.handleURL = function(contact, qualifiers, rawData) {
  var parser = this;
  return parser.handleGenericContactFieldList(contact, qualifiers, rawData, "urls");
};

/**
 * Handle reception of IMPP (website)
 *
 * @param {object} contact - The contact to update with new information
 * @param {object} qualifiers - Parsed kv/v qualifiers (ex. HOME, WORK)
 * @param {string} rawData - Data on the line
 */
VCFParser.prototype.handleIMPP = function(contact, qualifiers, rawData) {
  var parser = this;
  return parser.handleGenericContactFieldList(contact, qualifiers, rawData, "impp");
};

/**
 * Handle fields that produce a simple ContactField[]
 *
 * @param {object} contact - The contact to update with new information
 * @param {object} qualifiers - Parsed kv/v qualifiers (ex. HOME, WORK)
 * @param {string} rawData - Data on the line
 * @param {string} key - key in current contact that will be used to store the ContactField[]
 */
VCFParser.prototype.handleGenericContactFieldList = function(contact, qualifiers, rawData, key) {
  var parser = this;
  // Create a list of the types the url was qualified with
  var types = [];
  for (var t = 0; t < parser._VALID_TEL_TYPES.length; t++) {
    if (parser._VALID_TEL_TYPES in qualifiers) {
      types.push(parser._VALID_TEL_TYPES[t]);
    }
  }

  // Add the url address to the list of urls for this contact
  parser.state.currentContact[key].push({
    types: types,
    preferred: 'pref' in qualifiers,
    value: rawData
  });

};

/**
 * Decode a UTF-8 quoted printable string into a UTF8 string
 *
 * @param {string} str - UTF quoted printable string (ex. "=E8=97=A4=E6=A3=AE")
 */
function utf8QuotedPrintableDecode(str) {
  if (typeof str === "undefined") { return undefined; }

  // Grab the ascii bytes separated by "=", trim first/last elements if empty
  var asciiHex = str.split(/=+/);
  if (asciiHex[0] === "") { asciiHex.shift(); }
  if (asciiHex[asciiHex.length - 1] === "") { asciiHex.pop(); }

  // Convert ascii hex into bytes
  var bytes = [];
  for (var i = 0; i < asciiHex.length; i++) {
    bytes.push(String.fromCharCode(parseInt(asciiHex[i], 16)));
  }

  // Return UTF8 decoded string (trick outlined @ http://ecmanaut.blogspot.co.uk/2006/07/encoding-decoding-utf8-in-javascript.html)
  var utf8String = decodeURIComponent(escape(bytes.join("")));
  return utf8String;
}

export default VCFParser;
