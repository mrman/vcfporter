import REGEXPS from "vcfporter/helpers/regexps";

/**
 * Cross platform functionality
 *
 * @exports App.CROSS_PLATFORM
 */

/**
 * Function to search for all .vcf files in storage and push created file objects into the application store.
 *
 */
function findVcfFiles_ffos(store){
  // Exit early if getDeviceStorage is not available
  if (_.isUndefined(navigator) || _.isUndefined(navigator.getDeviceStorage)) { return; }

  // Look for VCF files on all sdcard storage
  var sdcards = navigator.getDeviceStorages("sdcard");

  _.each(sdcards, function(sdcard) {
    var cursor = sdcard.enumerate();

    cursor.onsuccess = function() {
      var file = this.result;
      if (file) {
        // Add to application store
        if (file.name.match(REGEXPS.vcf)) {
          var fileRecord = {
            id: file.name,
            fileObj: file,
            size: file.size,
            name: file.name,
            type: file.type,
            parsed: false,
            mozFullPath: file.mozFullPath,
            lastModifiedDate: file.lastModifiedDate
          };
          store.push('vcf-file', fileRecord);
        }

        // Get more results, if there are any
        if (!this.done) {
          this.continue();
        }

      }
    };

    cursor.onerror = function() {
      console.warn("No files found:", this.error);
    };
  });
}

/**
 * Save new contact
 *
 * @param {object} contact - Contact model instance
 * @param {object} successCb - Callback to call in case of successful save
 * @param {object} errorCb - Callback to call in case of failed save
 */
function createNewContact_ffos(contact, successCb, errorCb) {
  var mozContactData = contact.generateMozContact();
  // Create the contact (expects just the name property first)
  var person = new mozContact(mozContactData);
  if ('init' in person) { person.init(mozContactData); }

  var newContactSave = navigator.mozContacts.save(person);
  newContactSave.onsuccess = function() {
    if (successCb) { successCb(person); }
  };
  newContactSave.onerror = function(err) {
    if (errorCb) { successCb(err); }
  };
}

/**
 * Save selected contacts
 *
 * @param {list} contacts - Selected contacts for saving to device
 * @returns A promise that will be fulfilled when all contacts are saved
 */
function saveSelectedContacts_ffos(contacts) {
  var processedContacts = [];
  var deferred = $.Deferred();
  var promise = deferred.promise();

  _.each(contacts, function(c) {
    // Quit early if no displayable name is found
    var displayableName = c.get('displayableName');
    if (_.isUndefined(displayableName) || _.isNull(displayableName)) { return; }

    // Attempt to find the contact by displayName
    var searchOptions = {
      filterValue: displayableName,
      filterBy: ['givenName', 'name'],
      filterOp: 'contains',
      filterLimit: 1,
      sortBy: 'familyName',
      sortOrder: 'ascending'
    };

    var search = navigator.mozContacts.find(searchOptions);

    search.onsuccess = function() {
      if (search.result.length) {
        // Modify existing contact and re-save
        var existingContact = search.result[0];
        mergeContactData_ffos(existingContact, c.generateMozContact());
        navigator.mozContacts.save(existingContact);

        // Update processedContacts list and notify progress watchers
        processedContacts.push(existingContact);
        deferred.notify({status: "updated", contact: existingContact });

        // If this was the last contact, resolve callback
        if (processedContacts.length === contacts.length) { deferred.resolve(processedContacts); }

      } else {
        // Create new contact and push it into the processedContacts list, update progress watchers
        var newContact = createNewContact_ffos(c);
        processedContacts.push(newContact);
        deferred.notify({status: "created", contact: newContact });

        // If this was the last contact, resolve callback
        if (processedContacts.length === contacts.length) { deferred.resolve(processedContacts); }
      }
    };
    search.onerror = function() {
        // Create new contact and push it into the processedContacts list, update progress watchers
        var newContact = createNewContact_ffos(c);
        processedContacts.push(newContact);
        deferred.notify({status: "created", contact: newContact });

      // If this was the last contact, resolve callback
      if (processedContacts.length === contacts.length) { deferred.resolve(processedContacts); }
    };

  });

  return promise;
}

/**
 * Merge data from a second mozilla contact into the first
 *
 * @param {object} first - The contact object that will have data merged into it
 * @param {object} second - The (possibly partial) contact object that has new data to be merged in
 */
function mergeContactData_ffos(first, second) {

  // Merge simple array properties
  _(MERGE_LOGIC.simpleArray)
    .each(function(k) {
      // Handle relatively simple merges of string arrays
      first[k] = _.union(first[k], second[k]);
    });
}


/**
 * Cross-platform function dispatch
 */
var CROSS_PLATFORM_FN = {
  findVCFFiles: {ffos: findVcfFiles_ffos },
  saveSelectedContacts: {ffos: saveSelectedContacts_ffos },
  mergeContactData: {ffos: mergeContactData_ffos },
  createNewContact: {ffos: saveSelectedContacts_ffos }
};

// Merge logic complexity lookup for different fields in FF's stored contact
var MERGE_LOGIC = {
  simpleArray: ['familyName', 'givenName', 'honorificPrefix', 'honorificSuffix', 'jobTitle', 'nickname', 'name', 'org', 'photos', 'categories'],
};


export default CROSS_PLATFORM_FN;
