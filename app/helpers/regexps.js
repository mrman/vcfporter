/**
 * Regular expressons used in the application
 *
 * @exports REGEXPS
 */

// Application regular expression patterns
var REGEXPS = {
  vcf: /\.vcf$/
};

export default REGEXPS;
