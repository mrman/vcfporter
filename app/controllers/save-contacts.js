import Ember from "ember";
import CROSS_PLATFORM_FN from "vcfporter/helpers/cross-platform";
import App from "vcfporter/app";

var SaveContactsController = Ember.ArrayController.extend({
  /**
   * Save Contacts tab controller
   *
   * @exports SaveContactsController
   */

  actions: {

    saveSelectedContacts: function() {
      var controller = this;

      // Progress variables
      var contacts = controller.get('selectedContacts');
      var contactsProcessed = 0;
      var totalContacts = contacts.length;
      controller.set('progressShow', true);

      // Save the selected contact
      CROSS_PLATFORM_FN
        .saveSelectedContacts[App.PLATFORM](contacts)
        .progress(function() {
          // Normally would take info object here
          controller.set('progressAmount', ++contactsProcessed);
          controller.set('contactsProcessed', contactsProcessed);
          controller.set('progressAmount',
                         Math.floor((contactsProcessed * 1.0 / totalContacts) * 100));
        })
        .done(function() {
          // Normally would take contacts list here
          controller.set('successfullySavedContacts', true);
        })
        .fail(function() {
          // Normally would take error here
          controller.set('encounteredErrorSavingContacts', true);
        });
    }

  }
});

export default SaveContactsController;
