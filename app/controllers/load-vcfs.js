import Ember from "ember";
import Contact from "vcfporter/models/contact";
import VCFParser from "vcfporter/helpers/vcf-parser";

var LoadVcfsController = Ember.ArrayController.extend({
  /**
   * VCF load tab controller
   *
   * @exports LoadVcfsController
   */

  actions: {

    toggleSelectedFile: function(file) { file.toggleSelected(); },

    addVCFFromInput: function() {
      var store = this.store;
      var files = document.getElementById('hdd-vcf-file').files;

      // Add all the files to the store
      _.forEach(files, function(f) {
        var fileRecord = {id: 1,
                          fileObj: f,
                          size: f.size,
                          name: f.name,
                          type: f.type,
                          mozFullPath: f.mozFullPath,
                          lastModifiedDate: f.lastModifiedDate
                         };
        store.push('vcf-file', fileRecord);
      });

      // Update the vcf files on the page
      this.set('vcfFiles', this.store.all('vcf-file'));
    },

    loadVCards: function() {
      var controller = this;

      // Show the progress bar before switch
      var files = controller.get('vcfFiles').content;
      if (files.length) {
        controller
          .set('progressShow', true)
          .set('progressAmount', 0)
          .set('progressActivity', true);
      }

      Ember.run.later(function() {
        // Load contacts and switch
        _(files)
          .filter(function(f) { return f.get('selected') && !f.get("parsed"); })
          .tap(function(files) {
            // Go straight to select contacts screen if contacts are all parsed
            if (_.isEmpty(files)) {
              controller.set('progressShow', false);
              controller.transitionToRoute('select-contacts');
            }
          })
          .forEach(function(f) {
            // Call parse and populate the database
            var parser = new VCFParser({ requiredFields: ['name'] });
            parser.parse(f.get("fileObj"), function(contact) {
              // Add ID and push into contact store
              contact.id = Contact.prototype.generateSerialID();
              controller.store.push('contact', contact);
            }, function() {
              // Done callback, set parsed to true so we don't reparse later
              controller.set('progressShow', false);
              f.set('parsed', true);
              controller.transitionToRoute('select-contacts');
            });
          });

      }, 100); // /run.later
    }
  }
});

export default LoadVcfsController;
