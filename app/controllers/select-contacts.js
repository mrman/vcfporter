import Ember from "ember";

var SelectContactsController = Ember.ArrayController.extend({
  /**
   * Select Contacts tab controller
   *
   * @exports SelectContactsController
   */

  actions: {

    selectAll: function() {
      // Select all the contacts
      this.get('contacts').forEach(function(c) { c.set('selected', true); });
    },

    unselectAll: function() {
      // Unselect all the contacts
      this.get('contacts').forEach(function(c) { c.set('selected', false); });
    },

    toggleSelectedContact: function(contact) { contact.toggleSelected(); }

  }
});

export default SelectContactsController;
