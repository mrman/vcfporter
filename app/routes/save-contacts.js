import Ember from "ember";
import App from "vcfporter/app";

var SaveContactsRoute = Ember.Route.extend({
  /**
   * Select Contacts Route
   *
   * @exports SelectContactsRoute
   */

  setupController: function(controller) {

    // Handle cross-platform functionality
    switch (App.PLATFORM) {
    case "ffos":
      controller.set('platform_ffos', true);
      break;
    case "web":
      controller.set('platform_web', true);
      break;
    default:
      throw new Error("Unrecognized platform [" + App.PLATFORM + "] !");
    }

    controller.set('pageTitle', 'Save Contacts'); // Required by topnav partial

    // Get the saved contacts
    this.store
      .filter('contact', function(c) { return c.get('selected'); })
      .then(function(contacts) { controller.set('selectedContacts', contacts.content); });
  }
});

export default SaveContactsRoute;
