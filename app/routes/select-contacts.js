import Ember from "ember";

var SelectContactsRoute = Ember.Route.extend({
  /**
   * Select Contacts Route
   *
   * @exports SelectContactsRoute
   */

  setupController: function(controller) {
    // Parse any selected contact files that have not been parsed
    controller.set('pageTitle', 'Select Contacts'); // Required by topnav partial
    controller.set('contacts', this.store.all('contact'));
  }
});

export default SelectContactsRoute;
