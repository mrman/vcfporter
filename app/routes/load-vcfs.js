import Ember from "ember";
import CROSS_PLATFORM_FN from "vcfporter/helpers/cross-platform";
import App from "vcfporter/app";

var LoadVcfsRoute = Ember.Route.extend({
  /**
   * VCF load tab route
   *
   * @exports LoadVcfsRoute
   */
  setupController: function(controller) {
    // Perform platform specific handling
    switch (App.PLATFORM) {
      case "ffos":
        controller.set('platform_ffos', true);
        CROSS_PLATFORM_FN.findVCFFiles[App.PLATFORM](this.store);
        break;
      case "web":
        controller.set('platform_web', true);
        break;
      default:
        throw new Error("Unrecognized platform [" + App.PLATFORM + "] !");
    }

    controller.set('pageTitle', 'Load vCard Files'); // Required by topnav partial
    controller.set('vcfFiles', this.store.all('vcf-file'));
  }
});

export default LoadVcfsRoute;
