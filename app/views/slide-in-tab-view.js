import Ember from "ember";

var SlideInTabView = Ember.View.extend({
  /**
   * Tab view that makes tabs slide in
   *
   * @exports SlideInTabView
   */

  classNames: ["tab-container"],
    classNameBindings: ["preSlideIn:pre-slide-in","slideIn:slide-in-left"],
    preSlideIn: true,
    slideIn: false,

    didInsertElement: function() {
      var self = this;

      Ember.run.next(this,function() {
        setTimeout(function() {
          self.set("preSlideIn",false);
          self.set("slideIn",true);
        },10);
      });
    }
});

export default SlideInTabView;
