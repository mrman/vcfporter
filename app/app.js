import Ember from 'ember';
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';

Ember.MODEL_FACTORY_INJECTIONS = true;

var App = Ember.Application.extend({
  modulePrefix: 'vcfporter', // TODO: loaded via config
  Resolver: Resolver
});

// Localforage adapter setup (BROKEN for now)
// App.ApplicationSerializer = DS.LFSerializer.extend();
// App.ApplicationAdapter = DS.LFAdapter.extend({
//   namespace: 'vcf-porter'
// });

// Detect platform
App.PLATFORM = navigator.getDeviceStorages ? "ffos" : "web";

// Global user information
App.currentUser = {
  selectedFiles: new Ember.Set(),
  selectedContacts: new Ember.Set()
};

loadInitializers(App, 'vcfporter');

export default App;
