/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var pickFiles = require('broccoli-static-compiler');
var mergeTrees = require('broccoli-merge-trees');
var Funnel = require('broccoli-funnel');

var app = new EmberApp({
  wrapInEval: false
});

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

// Lodash
app.import({
  development: 'bower_components/lodash/dist/lodash.js',
  production: 'bower_components/lodash/dist/lodash.min.js'
});

// LocalForage Adapter
//app.import('vendor/ember-localforage-adapter/localforage_adapter.js');


// CSS bower imports

// Firefox OS Building Blocks
var buildingBlocksCSS = pickFiles('bower_components', {
  srcDir: 'building-blocks',
  files: [
    'style/headers.css',
    'style/buttons.css',
    'style/tabs.css',
    'style/toolbars.css',
    'style/lists.css',
    'style/progress_activity.css',
    'style/switches.css',
    'style/switches/images/*',
    'transitions.css',
    'util.css',
    'fonts.css'
  ],
  destDir: '/assets/building-blocks'
});

var buildingBlocksCSS

// PureCSS
var pureCSS = pickFiles('bower_components', {
  srcDir: 'pure',
  files: [
    'pure-min.css',
    'grids-responsive-min.css'
  ],
  destDir: '/assets/pure'
});

// Gaia Icons
var gaiaIcons = pickFiles('bower_components', {
  srcDir: 'gaia-icons',
  files: [
    'fonts/*',
    'gaia-icons-embedded.css'
  ],
  destDir: '/assets/gaia-icons'
});

// Copy pre-start and start scripts to get around FFOS's CSP restriction banning inline scripts
// These files are stored in shim/ folder
var shims = new Funnel("shims", {
  srcDir: '.',
  destDir: 'assets'
});

// Copy manifest (single file pattern with broccoli-funnel)
var manifest = new Funnel("manifests", {
  srcDir: '.',
  getDestinationPath: function(p) {
    if (p === "manifest.firefoxos.json") { return "manifest.webapp"; }
    return p;
  }
});

// Copy icons (single file pattern with broccoli-funnel)
var icons = new Funnel("icons", {
  srcDir: '.',
  getDestinationPath: function(p) {
    if (p === "manifest.firefoxos.json") { return "manifest.webapp"; }
    return p;
  },
  destDir: 'assets/images/icons'
});

var emberApp = app.toTree();

module.exports = mergeTrees([emberApp, buildingBlocksCSS, pureCSS, gaiaIcons, shims, manifest, icons], {
  overwrite: true
});
