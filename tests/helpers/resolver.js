import Resolver from 'ember/resolver';

var resolver = Resolver.create();

resolver.namespace = {
  modulePrefix: 'vcfporter'
};

export default resolver;
